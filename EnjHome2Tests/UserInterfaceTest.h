#include <gtest\gtest.h>
#include "../EnjHome2Tests//RoomBaseMock.h"
#include "../EnjHome2Tests/RentControllerMock.h"
#include "../EnjHom2Lib/UserInterface.h"
#pragma once
class UserInterfaceTest: public ::testing::Test
{

protected:

	RoomBaseMock* roomBaseMock = new RoomBaseMock();
	RentControllerMock* rentControllerMock = new RentControllerMock();
	UserInterface* userInterface = new UserInterface(roomBaseMock, rentControllerMock);
	vector<Room*> rooms = vector<Room*>();
	int rentDays = 10;
	Room* room = new Room("adress", 1, 2, 30);

	virtual void SetUp()
	{
		userInterface = new UserInterface(roomBaseMock, rentControllerMock);
		time_t timeNow = time(0);
		tm* startDate = new tm();
		tm* endDate = new tm();
		localtime_s(startDate, &timeNow);
		localtime_s(endDate, &timeNow);
		endDate->tm_mday += rentDays;

		room = new Room("adress", 1, 2, 30);
		rooms.push_back(room);

		userInterface->currentRoomList = rooms;
		userInterface->rentDays = 10;
		userInterface->startDate = startDate;
		userInterface->endDate = endDate;
		userInterface->selectedRoomIndex = 0;
	}

	virtual void TearDown()
	{
	}

};

