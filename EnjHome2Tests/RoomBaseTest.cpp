#include "RoomBaseTest.h"

TEST_F(RoomBaseTest, findAllTest)
{
	vector<Room*> rooms = roomBase->findAll();
	EXPECT_EQ(rooms.size(), 2);
	EXPECT_EQ(rooms [0]->id, firstRoomID);
	EXPECT_EQ(rooms [1]->id, secRoomID);
}

TEST_F(RoomBaseTest, findByRentedSortedByAddress)
{
	vector<Room*> rooms = roomBase->findByRentedSortedByAddress(false);
	EXPECT_EQ(rooms.size(), 2);
	EXPECT_TRUE(rooms [0]->address > rooms [1]->address);
}

TEST_F(RoomBaseTest, sortedList)
{
	vector<Room*> rooms = roomBase->findByRentedSortedByAddress(true);
	EXPECT_EQ(rooms.size(), 0);
}

TEST_F(RoomBaseTest, findByRentedAndStreetAndMoneyAndSquare)
{
	vector<Room*> rooms = roomBase->findByConditions(false, "a", 2, 3);
	EXPECT_EQ(rooms.size(), 1);
	EXPECT_EQ(rooms [0]->address, "a");
	EXPECT_EQ(rooms [0]->square, 2);
	EXPECT_EQ(rooms [0]->moneyPerMonth, 3);
}

TEST_F(RoomBaseTest, findAndUpdate)
{
	tm startRentDate = { 0 };
	tm endRentDate = { 0 };

	Room* updatedRoom = new Room("new", 100, 200, 300);
	updatedRoom->id = firstRoomID;
	Renter* renter = new Renter("first", "last", "fath");
	updatedRoom->renter = renter;

	time_t timeNow = time(0);
	localtime_s(&startRentDate, &timeNow);
	localtime_s(&endRentDate, &timeNow);
	endRentDate.tm_mday += 10;

	updatedRoom->rentStartDate = startRentDate;
	updatedRoom->rentEndDate = endRentDate;

	roomBase->fingByIdAndUpdate(firstRoomID, updatedRoom);
	vector<Room*> rooms = roomBase->findAll();
	EXPECT_EQ(rooms.size(), 2);
	EXPECT_EQ(rooms [0]->id, firstRoomID);
	EXPECT_EQ(rooms [0]->address, updatedRoom->address);
	EXPECT_EQ(rooms [0]->square, updatedRoom->square);
	EXPECT_EQ(rooms [0]->moneyPerMonth, updatedRoom->moneyPerMonth);

}
