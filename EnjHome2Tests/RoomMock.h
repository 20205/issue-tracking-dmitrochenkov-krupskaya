#pragma once
#include <iostream>
#include "../EnjHom2Lib/Room.h"
#include "gmock/gmock.h"

class RoomMock: public Room
{

public:
	RoomMock(string address, int houseNumber, int square, int moneyPerMonth) : Room(address, houseNumber, square, moneyPerMonth){}
	MOCK_METHOD3(startRent, void(Renter* renter, tm* rentStartDate, int rentedDays));
	MOCK_METHOD0(endRent, void());
	MOCK_METHOD1(serealize, void(ostream& ofStream));
	MOCK_METHOD1(deserealize, Room*(istream& ofStream));
	MOCK_METHOD0(getTextCoreData, string());
};