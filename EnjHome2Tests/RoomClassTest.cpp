#include "RoomClassTest.h"

TEST_F(RoomClassTest, saveLoadRoomFile)
{

	ofstream fileOfstream(testFilenmae, ios::binary);
	room->serealize(fileOfstream);
	fileOfstream.close();

	ifstream fileIfstream(testFilenmae, ios::binary);
	Room* deserializedRoom = new Room();
	deserializedRoom = deserializedRoom->deserealize(fileIfstream);
	fileIfstream.close();

	EXPECT_EQ(deserializedRoom->address, room->address) << "wrong room adress";
	EXPECT_EQ(deserializedRoom->houseNumber, room->houseNumber) << "wrong room houce number";
	EXPECT_EQ(deserializedRoom->square, room->square) << "wrong room square";
	EXPECT_EQ(deserializedRoom->renter->firstName, room->renter->firstName) << "wrong room renter first name";
	EXPECT_EQ(deserializedRoom->renter->lastName, room->renter->lastName) << "wrong room renter last name";
	EXPECT_EQ(deserializedRoom->renter->fath, room->renter->fath) << "wrong room renter fath";
	EXPECT_EQ(mktime(&deserializedRoom->rentStartDate), mktime(&room->rentStartDate)) << "wrong room rent start date";
	EXPECT_EQ(mktime(&deserializedRoom->rentEndDate), mktime(&room->rentEndDate)) << "wrong room rent end date";
}

TEST_F(RoomClassTest, startRent)
{
	int daysCount = 10;

	room->renter = NULL;
	room->startRent(renter, &startRentDate, daysCount);

	time_t timeNow = time(0);
	tm* endDate = &startRentDate;
	localtime_s(endDate, &timeNow);
	endDate->tm_mday += daysCount;
	timeNow = mktime(endDate);
	localtime_s(endDate, &timeNow);

	EXPECT_TRUE(room->isRented) << "wrong isRented room status";
	EXPECT_EQ(room->renter, renter) << "wrong room renter";
	EXPECT_EQ(room->rentedDays, 10) << "wrong rent days count";
	EXPECT_EQ(mktime(&room->rentEndDate), mktime(endDate)) << "wrong room rent end date";
}

TEST_F(RoomClassTest, endRent)
{
	room->endRent();

	EXPECT_FALSE(room->isRented) << "wrong isRented room status";
	EXPECT_TRUE(room->renter == NULL) << "wrong room renter";
	EXPECT_EQ(room->rentedDays, -1) << "wrong rent days count";
}