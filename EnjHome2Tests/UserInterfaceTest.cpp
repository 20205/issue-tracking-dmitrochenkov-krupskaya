#include "UserInterfaceTest.h"
using namespace testing;

TEST_F(UserInterfaceTest, getAllRooms)
{
	EXPECT_CALL(*roomBaseMock, findAll()).Times(1);
	userInterface->getAllRooms();
}

TEST_F(UserInterfaceTest, searchRoomByStreetAndMoneyAndSqure)
{
	EXPECT_CALL(*roomBaseMock, findByRentedAndStreetAndMoneyAndSquare(false, "adress", 1, 2)).Times(1);
	userInterface->searchRoom(false, "adress", 1, 2);
}

TEST_F(UserInterfaceTest, takeRoom)
{
	EXPECT_CALL(*rentControllerMock, payForRent(10, room)).Times(1);
	Renter* renter = new Renter("1", "2", "3");
	userInterface->takeRoom(renter, userInterface->startDate, userInterface->rentDays);

	EXPECT_EQ(userInterface->selectedRoomIndex, -1);
	EXPECT_EQ(userInterface->currentRoomList.size(), 0);
}

TEST_F(UserInterfaceTest, getCurrentRoomCost_1)
{
	double cost = userInterface->getCurrentRoomCost(userInterface->startDate, userInterface->rentDays);
	EXPECT_EQ(cost, 10);
}

TEST_F(UserInterfaceTest, getCurrentRoomCost_2)
{
	userInterface->rentDays = 20;
	double cost = userInterface->getCurrentRoomCost(userInterface->startDate, userInterface->rentDays);
	EXPECT_EQ(cost, 20);
}

TEST_F(UserInterfaceTest, getCurrentRoomCost_3)
{
	int dateDiff = 10;
	userInterface->rentDays = 10;
	userInterface->startDate->tm_mday += dateDiff;
	userInterface->endDate->tm_mday += dateDiff;
	double cost = userInterface->getCurrentRoomCost(userInterface->startDate, userInterface->rentDays);
	EXPECT_EQ(cost, userInterface->rentDays * (double)room->moneyPerMonth / 30 + (double)room->moneyPerMonth / 30 * (double)dateDiff / 100 * 10);
}

