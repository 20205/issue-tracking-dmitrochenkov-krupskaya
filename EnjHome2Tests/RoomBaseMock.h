#include "gmock/gmock.h"
#include "../EnjHom2Lib/Room.h"
#include "../EnjHom2Lib/RoomBase.h"
#pragma once
class RoomBaseMock: public RoomBase
{
public:
    MOCK_METHOD2(fingByIdAndUpdate, bool(int id, Room* room));
	MOCK_METHOD0(findAll, vector<Room>());
	MOCK_METHOD4(findByRentedAndStreetAndMoneyAndSquare, vector<Room>(bool rented, string adress, int money, int square));
	MOCK_METHOD1(findByRentedSortedByAddress, vector<Room>(bool rented));
};