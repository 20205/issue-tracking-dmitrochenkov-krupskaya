#include <gtest\gtest.h>
#include <fstream>
#include <iostream>
#include "../EnjHome2Tests/RoomBaseMock.h"
#include "../EnjHom2Lib/RentController.h"
#include "../EnjHome2Tests/RoomMock.h"
#include "../EnjHome2Tests/RenterMock.h"
#pragma once
class RentControllerTest: public ::testing::Test
{

protected:

	Renter* renter = NULL;
	RentController* rentController = new RentController();
	double money = 1000.0;
	int rentDays = 10;
	tm startRentDate = { 0 };
	tm endRentDate = { 0 };


	virtual void SetUp()
	{
		renter = new RenterMock("first", "last", "fath");
	}

	virtual void TearDown()
	{
		delete(renter);
		delete(rentController);
	}

};

