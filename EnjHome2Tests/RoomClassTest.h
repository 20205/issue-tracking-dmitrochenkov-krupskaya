#include <gtest\gtest.h>
#include <fstream>
#include <iostream>
#include "../EnjHom2Lib/Room.h"
#pragma once
class RoomClassTest: public ::testing::Test
{

protected:

	string testFilenmae = "testFile.dat";
	Room* room = NULL;
	Renter* renter = NULL;
	tm startRentDate = { 0 };
	tm endRentDate = { 0 };

	virtual void SetUp()
	{
		room = new Room("1", 1, 2, 3);
		renter = new Renter("first", "last", "fath");
		room->renter = renter;

		time_t timeNow = time(0);
		localtime_s(&startRentDate, &timeNow);
		localtime_s(&endRentDate, &timeNow);
		endRentDate.tm_mday += 10;

		room->rentStartDate = startRentDate;
		room->rentEndDate = endRentDate;
	}

	virtual void TearDown()
	{
		delete(renter);
		delete(room);
	}

};

