#include <gtest\gtest.h>
#include <fstream>
#include <iostream>
#include "../EnjHom2Lib/Room.h"
#include "../EnjHom2Lib/RoomBase.h"
#include <gtest\gtest.h>
#pragma once

class RoomBaseTest: public ::testing::Test
{

protected:

	RoomBase* roomBase = new RoomBase();
	int firstRoomID = 1;
	int secRoomID = 2;

	virtual void SetUp()
	{
		remove(roomBase->baseFilename.c_str());

		tm startRentDate = { 0 };
		tm endRentDate = { 0 };

		Room* room = new Room("a", 1, 2, 3);
		room->id = firstRoomID;
		Renter* renter = new Renter("first", "last", "fath");
		room->renter = renter;

		time_t timeNow = time(0);
		localtime_s(&startRentDate, &timeNow);
		localtime_s(&endRentDate, &timeNow);
		endRentDate.tm_mday += 10;

		room->rentStartDate = startRentDate;
		room->rentEndDate = endRentDate;

		Room* room1 = new Room("b", 11, 22, 33);
		room1->id = secRoomID;
		renter = new Renter("first(2)", "last(2)", "fath(2)");
		room1->renter = renter;

		localtime_s(&startRentDate, &timeNow);
		localtime_s(&endRentDate, &timeNow);
		endRentDate.tm_mday += 10;

		room1->rentStartDate = startRentDate;
		room1->rentEndDate = endRentDate;

		ofstream fileOfstream(roomBase->baseFilename, ios::binary);

		room->serealize(fileOfstream);
		room1->serealize(fileOfstream);
	}

	virtual void TearDown()
	{
	}

};

