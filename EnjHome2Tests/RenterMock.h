#pragma once
#include <iostream>
#include "../EnjHom2Lib/Room.h"
#include "gmock/gmock.h"

class RenterMock: public Renter
{
public:
	RenterMock(string firstName, string lastName, string fath) : Renter(firstName, lastName, fath){}
	MOCK_METHOD1(serealize, void (ostream& ofStream));
	MOCK_METHOD1(deserealize, Renter* (istream& ofStream));
	MOCK_METHOD0(getTextCoreData, string ());
};