#include "../EnjHom2Lib/RentController.h"
#include "../EnjHom2Lib/Room.h"
#include "gmock/gmock.h"
#pragma once

class RentControllerMock : public RentController
{

public:
	MOCK_METHOD2(payForRent, bool(double money, Room* room));

};

