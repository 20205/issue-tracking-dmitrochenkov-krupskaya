#pragma once

namespace GUI
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm1
	/// </summary>
	public ref class LastDealForm : public System::Windows::Forms::Form
	{
	public:
		LastDealForm(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~LastDealForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^ OKBtn;
	protected:


	private: System::Windows::Forms::RichTextBox^ coreTextBox;

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->OKBtn = (gcnew System::Windows::Forms::Button());
			this->coreTextBox = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// OKBtn
			// 
			this->OKBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->OKBtn->Location = System::Drawing::Point(631, 654);
			this->OKBtn->Name = L"OKBtn";
			this->OKBtn->Size = System::Drawing::Size(125, 38);
			this->OKBtn->TabIndex = 1;
			this->OKBtn->Text = L"�������";
			this->OKBtn->UseVisualStyleBackColor = true;
			this->OKBtn->Click += gcnew System::EventHandler(this, &LastDealForm::button1_Click);
			// 
			// coreTextBox
			// 
			this->coreTextBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->coreTextBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->coreTextBox->Location = System::Drawing::Point(13, 13);
			this->coreTextBox->Name = L"coreTextBox";
			this->coreTextBox->ReadOnly = true;
			this->coreTextBox->Size = System::Drawing::Size(743, 632);
			this->coreTextBox->TabIndex = 2;
			this->coreTextBox->Text = L"";
			// 
			// MyForm1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(9, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(768, 701);
			this->Controls->Add(this->coreTextBox);
			this->Controls->Add(this->OKBtn);
			this->Name = L"MyForm1";
			this->Text = L"������� ������";
			this->ResumeLayout(false);

		}
#pragma endregion
	public: void updateCoreText(String^ text)
	{
		this->coreTextBox->Text = text;
	}
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e)
	{
	
		this->Visible = false;

	}
	};
}
