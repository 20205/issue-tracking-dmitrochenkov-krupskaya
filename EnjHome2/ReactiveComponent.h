#pragma once
#include <string>
#include <vector>
using namespace std;

class ReactiveComponent
{

public:

	string defaultInvalidMessage = "";
	string invalidMessage = "";
	string value = "";
	bool valid = false;
	int subscriptionsCount = 0;
	vector<ReactiveComponent*> suscribers = vector<ReactiveComponent*>(0);

	ReactiveComponent(string defaultInvalidMessage)
	{
		this->defaultInvalidMessage = defaultInvalidMessage;
	}

	/**
	* Update component value
	*
	* @param newValue new component value
	**/
	void update(string newValue)
	{
		this->value = newValue;
		validate();
		for(int i = 0; i < this->suscribers.size(); i++)
		{
			this->suscribers [i]->notify(this->valid);
		}
	}

	/**
	* Get notifications from other component about it's valid
	*
	* @param reactiveField component-subscription
	**/
	void subscribe(ReactiveComponent* reactiveField)
	{
		this->subscriptionsCount++;
		reactiveField->suscribers.push_back(this);
	}

	/**
	* Reaction to notification from components-subscriptions
	*
	* @param otherFieldIsValid is component-subscription has valid value
	**/
	virtual void notify(bool otherFieldIsValid)
	{
		if(!otherFieldIsValid)
		{
			this->valid = false;
			this->invalidMessage = this->defaultInvalidMessage;
		}
	}

	/**
	* Validate current component value
	**/
	virtual void validate() = 0;


};

