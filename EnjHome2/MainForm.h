#include "../EnjHom2Lib/UserInterface.h"
#include "../EnjHom2Lib/ValidationUtills.h"
#include "../EnjHom2Lib/ConvertUtills.h"
#include <regex>
#include "ReactiveComponent.h"
#include "RentDaysField.h"
#include "PositiveNumberField.h"
#include "NonEmptyTextField.h"
#include "DateTextField.h"
#include "LastDealForm.h"
#include "AdminForm.h"


namespace GUI
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class MainForm: public System::Windows::Forms::Form
	{
	private: System::Windows::Forms::GroupBox^ rentProcessBox;
	private: System::Windows::Forms::MaskedTextBox^ fathTextField;
	private: System::Windows::Forms::MaskedTextBox^ lastNameTextField;
	private: System::Windows::Forms::MaskedTextBox^ firstNameTextField;
	private: System::Windows::Forms::RichTextBox^ endDateTextField;
	private: System::Windows::Forms::Label^ finalCostLabel;
	private: System::Windows::Forms::Label^ endRentDateLabel;
	private: System::Windows::Forms::Label^ rentDaysLabel;
	private: System::Windows::Forms::TextBox^ rentDaysTextField;
	private: System::Windows::Forms::Label^ startRentDateLabel;
	private: System::Windows::Forms::TextBox^ startDateTextField;
	private: System::Windows::Forms::Label^ fathLabel;
	private: System::Windows::Forms::Label^ lastNameLabel;
	private: System::Windows::Forms::Label^ firstNameLabel;
	private: System::Windows::Forms::GroupBox^ searchBox;
	private: System::Windows::Forms::MaskedTextBox^ minCostTextField;
	private: System::Windows::Forms::MaskedTextBox^ minSqureTextField;
	private: System::Windows::Forms::Label^ minCostLabel;
	private: System::Windows::Forms::Button^ searchBtn;
	private: System::Windows::Forms::Label^ minSquareLabel;
	private: System::Windows::Forms::Label^ adressLabel;
	private: System::Windows::Forms::TextBox^ adressTextField;
	private: System::Windows::Forms::GroupBox^ rommListBox;
	private: System::Windows::Forms::ListView^ roomList;
	private: System::Windows::Forms::ColumnHeader^ id;
	private: System::Windows::Forms::ColumnHeader^ address;
	private: System::Windows::Forms::ColumnHeader^ sqare;
	private: System::Windows::Forms::ColumnHeader^ cost;

	private: System::ComponentModel::Container^ components;


	public:

		UserInterface* userInterface = new UserInterface(new RoomBase(), new RentController());

		ReactiveComponent* firstNameReactField = new NonEmptyTextField("���� '���' �� ����� ���� ������!");
		ReactiveComponent* lastNameReactField = new NonEmptyTextField("���� '�������' �� ����� ���� ������!");
		ReactiveComponent* fathReactField = new NonEmptyTextField("���� '��������' �� ����� ���� ������!");

		ReactiveComponent* rentDaysReactField = new RentDaysField("�������� ���� ������!");
		ReactiveComponent* finalCostReactField = new PositiveNumberField("������ �����!");
		ReactiveComponent* chosenRoomIndexReactField = new PositiveNumberField("�������� �� �������!");

		ReactiveComponent* startRentDateReactField = new DateTextField("�������� ����!");
	private: System::Windows::Forms::ColumnHeader^ roomsCount;
	public:

	private: System::Windows::Forms::ColumnHeader^ wifi;
	private: System::Windows::Forms::Label^ MinRoomsLabel;


	private: System::Windows::Forms::Button^ showHistoryBtn;


	private: System::Windows::Forms::Button^ rentBtn;
	private: System::Windows::Forms::RichTextBox^ finalCostTextField;
	private: System::Windows::Forms::Label^ renterDataLabel;

	private: System::Windows::Forms::ComboBox^ roomsCountSelect;


	private: System::Windows::Forms::ColumnHeader^ badsCount;
	private: System::Windows::Forms::ColumnHeader^ parking;
	private: System::Windows::Forms::CheckBox^ parkingCheckBox;

	private: System::Windows::Forms::CheckBox^ wifiCheckBox;
	private: System::Windows::Forms::Label^ badsCountLabel;

	private: System::Windows::Forms::ComboBox^ badsCountSelect;
	private: System::Windows::Forms::Button^ adminBtn;

	public:


		ReactiveComponent* endRentDateReactField = new DateTextField("������ �����!");
		AdminForm^ adminForm;

		MainForm(void)
		{
			this->finalCostReactField->subscribe(this->rentDaysReactField);
			this->finalCostReactField->subscribe(this->startRentDateReactField);
			this->finalCostReactField->subscribe(this->chosenRoomIndexReactField);

			endRentDateReactField->subscribe(rentDaysReactField);
			endRentDateReactField->subscribe(startRentDateReactField);

			time_t timeNow = time(0);
			tm* time = new tm();
			localtime_s(time, &timeNow);

			this->chosenRoomIndexReactField->update("");
			this->rentDaysReactField->update("");

			this->firstNameReactField->update("");
			this->lastNameReactField->update("");
			this->fathReactField->update("");

			InitializeComponent();

			updateButtonState();

			userInterface->getAllRooms();
			updateRoomList();

			this->startDateTextField->Text = ConvertUtills::dateToTextBoxString(time);
			this->rentDaysTextField->Text = "";
			delete(time);

		}

	protected:
		~MainForm()
		{
			delete(userInterface);
			delete(firstNameReactField);
			delete(lastNameReactField);
			delete(fathReactField);
			delete(rentDaysReactField);
			delete(finalCostReactField);
			delete(chosenRoomIndexReactField);
			if(components)
			{
				delete components;
			}
		}

	private:
#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->rentProcessBox = (gcnew System::Windows::Forms::GroupBox());
			this->renterDataLabel = (gcnew System::Windows::Forms::Label());
			this->finalCostTextField = (gcnew System::Windows::Forms::RichTextBox());
			this->rentBtn = (gcnew System::Windows::Forms::Button());
			this->fathTextField = (gcnew System::Windows::Forms::MaskedTextBox());
			this->lastNameTextField = (gcnew System::Windows::Forms::MaskedTextBox());
			this->firstNameTextField = (gcnew System::Windows::Forms::MaskedTextBox());
			this->endDateTextField = (gcnew System::Windows::Forms::RichTextBox());
			this->finalCostLabel = (gcnew System::Windows::Forms::Label());
			this->endRentDateLabel = (gcnew System::Windows::Forms::Label());
			this->rentDaysLabel = (gcnew System::Windows::Forms::Label());
			this->rentDaysTextField = (gcnew System::Windows::Forms::TextBox());
			this->startRentDateLabel = (gcnew System::Windows::Forms::Label());
			this->startDateTextField = (gcnew System::Windows::Forms::TextBox());
			this->fathLabel = (gcnew System::Windows::Forms::Label());
			this->lastNameLabel = (gcnew System::Windows::Forms::Label());
			this->firstNameLabel = (gcnew System::Windows::Forms::Label());
			this->searchBox = (gcnew System::Windows::Forms::GroupBox());
			this->badsCountLabel = (gcnew System::Windows::Forms::Label());
			this->badsCountSelect = (gcnew System::Windows::Forms::ComboBox());
			this->parkingCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->minCostTextField = (gcnew System::Windows::Forms::MaskedTextBox());
			this->wifiCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->roomsCountSelect = (gcnew System::Windows::Forms::ComboBox());
			this->MinRoomsLabel = (gcnew System::Windows::Forms::Label());
			this->minCostLabel = (gcnew System::Windows::Forms::Label());
			this->minSqureTextField = (gcnew System::Windows::Forms::MaskedTextBox());
			this->searchBtn = (gcnew System::Windows::Forms::Button());
			this->minSquareLabel = (gcnew System::Windows::Forms::Label());
			this->adressLabel = (gcnew System::Windows::Forms::Label());
			this->adressTextField = (gcnew System::Windows::Forms::TextBox());
			this->rommListBox = (gcnew System::Windows::Forms::GroupBox());
			this->roomList = (gcnew System::Windows::Forms::ListView());
			this->id = (gcnew System::Windows::Forms::ColumnHeader());
			this->address = (gcnew System::Windows::Forms::ColumnHeader());
			this->sqare = (gcnew System::Windows::Forms::ColumnHeader());
			this->roomsCount = (gcnew System::Windows::Forms::ColumnHeader());
			this->badsCount = (gcnew System::Windows::Forms::ColumnHeader());
			this->wifi = (gcnew System::Windows::Forms::ColumnHeader());
			this->parking = (gcnew System::Windows::Forms::ColumnHeader());
			this->cost = (gcnew System::Windows::Forms::ColumnHeader());
			this->showHistoryBtn = (gcnew System::Windows::Forms::Button());
			this->adminBtn = (gcnew System::Windows::Forms::Button());
			this->rentProcessBox->SuspendLayout();
			this->searchBox->SuspendLayout();
			this->rommListBox->SuspendLayout();
			this->SuspendLayout();
			// 
			// rentProcessBox
			// 
			this->rentProcessBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->rentProcessBox->Controls->Add(this->renterDataLabel);
			this->rentProcessBox->Controls->Add(this->finalCostTextField);
			this->rentProcessBox->Controls->Add(this->rentBtn);
			this->rentProcessBox->Controls->Add(this->fathTextField);
			this->rentProcessBox->Controls->Add(this->lastNameTextField);
			this->rentProcessBox->Controls->Add(this->firstNameTextField);
			this->rentProcessBox->Controls->Add(this->endDateTextField);
			this->rentProcessBox->Controls->Add(this->finalCostLabel);
			this->rentProcessBox->Controls->Add(this->endRentDateLabel);
			this->rentProcessBox->Controls->Add(this->rentDaysLabel);
			this->rentProcessBox->Controls->Add(this->rentDaysTextField);
			this->rentProcessBox->Controls->Add(this->startRentDateLabel);
			this->rentProcessBox->Controls->Add(this->startDateTextField);
			this->rentProcessBox->Controls->Add(this->fathLabel);
			this->rentProcessBox->Controls->Add(this->lastNameLabel);
			this->rentProcessBox->Controls->Add(this->firstNameLabel);
			this->rentProcessBox->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->rentProcessBox->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->rentProcessBox->Location = System::Drawing::Point(1016, 107);
			this->rentProcessBox->Margin = System::Windows::Forms::Padding(2);
			this->rentProcessBox->Name = L"rentProcessBox";
			this->rentProcessBox->Padding = System::Windows::Forms::Padding(2);
			this->rentProcessBox->Size = System::Drawing::Size(345, 491);
			this->rentProcessBox->TabIndex = 7;
			this->rentProcessBox->TabStop = false;
			this->rentProcessBox->Text = L"������� ������";
			// 
			// renterDataLabel
			// 
			this->renterDataLabel->AutoSize = true;
			this->renterDataLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 14.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->renterDataLabel->Location = System::Drawing::Point(8, 28);
			this->renterDataLabel->Name = L"renterDataLabel";
			this->renterDataLabel->Size = System::Drawing::Size(144, 23);
			this->renterDataLabel->TabIndex = 28;
			this->renterDataLabel->Text = L"������� ���:";
			// 
			// finalCostTextField
			// 
			this->finalCostTextField->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->finalCostTextField->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->finalCostTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 21, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->finalCostTextField->ForeColor = System::Drawing::SystemColors::InactiveCaptionText;
			this->finalCostTextField->Location = System::Drawing::Point(2, 418);
			this->finalCostTextField->Margin = System::Windows::Forms::Padding(2);
			this->finalCostTextField->Multiline = false;
			this->finalCostTextField->Name = L"finalCostTextField";
			this->finalCostTextField->ReadOnly = true;
			this->finalCostTextField->Size = System::Drawing::Size(341, 38);
			this->finalCostTextField->TabIndex = 27;
			this->finalCostTextField->Text = L"";
			// 
			// rentBtn
			// 
			this->rentBtn->BackColor = System::Drawing::Color::LightGreen;
			this->rentBtn->Dock = System::Windows::Forms::DockStyle::Bottom;
			this->rentBtn->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->rentBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->rentBtn->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->rentBtn->Location = System::Drawing::Point(2, 456);
			this->rentBtn->Margin = System::Windows::Forms::Padding(2);
			this->rentBtn->Name = L"rentBtn";
			this->rentBtn->Size = System::Drawing::Size(341, 33);
			this->rentBtn->TabIndex = 28;
			this->rentBtn->Text = L"�������� ������";
			this->rentBtn->UseVisualStyleBackColor = true;
			this->rentBtn->Click += gcnew System::EventHandler(this, &MainForm::rentBtn_Click);
			// 
			// fathTextField
			// 
			this->fathTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->fathTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->fathTextField->HidePromptOnLeave = true;
			this->fathTextField->Location = System::Drawing::Point(92, 129);
			this->fathTextField->Margin = System::Windows::Forms::Padding(2);
			this->fathTextField->Mask = L"L\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?";
			this->fathTextField->Name = L"fathTextField";
			this->fathTextField->Size = System::Drawing::Size(249, 27);
			this->fathTextField->TabIndex = 23;
			this->fathTextField->TextChanged += gcnew System::EventHandler(this, &MainForm::fathTextField_TextChanged);
			// 
			// lastNameTextField
			// 
			this->lastNameTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->lastNameTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lastNameTextField->HidePromptOnLeave = true;
			this->lastNameTextField->Location = System::Drawing::Point(92, 92);
			this->lastNameTextField->Margin = System::Windows::Forms::Padding(2);
			this->lastNameTextField->Mask = L"L\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?";
			this->lastNameTextField->Name = L"lastNameTextField";
			this->lastNameTextField->Size = System::Drawing::Size(249, 27);
			this->lastNameTextField->TabIndex = 22;
			this->lastNameTextField->TextChanged += gcnew System::EventHandler(this, &MainForm::lastNameTextField_TextChanged);
			// 
			// firstNameTextField
			// 
			this->firstNameTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->firstNameTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->firstNameTextField->HidePromptOnLeave = true;
			this->firstNameTextField->Location = System::Drawing::Point(92, 58);
			this->firstNameTextField->Margin = System::Windows::Forms::Padding(2);
			this->firstNameTextField->Mask = L"L\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?\?";
			this->firstNameTextField->Name = L"firstNameTextField";
			this->firstNameTextField->Size = System::Drawing::Size(249, 27);
			this->firstNameTextField->TabIndex = 21;
			this->firstNameTextField->TextChanged += gcnew System::EventHandler(this, &MainForm::firstNameTextField_TextChanged);
			// 
			// endDateTextField
			// 
			this->endDateTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->endDateTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 14.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->endDateTextField->Location = System::Drawing::Point(10, 327);
			this->endDateTextField->Margin = System::Windows::Forms::Padding(6, 2, 2, 2);
			this->endDateTextField->Multiline = false;
			this->endDateTextField->Name = L"endDateTextField";
			this->endDateTextField->ReadOnly = true;
			this->endDateTextField->Size = System::Drawing::Size(181, 29);
			this->endDateTextField->TabIndex = 26;
			this->endDateTextField->Text = L"";
			// 
			// finalCostLabel
			// 
			this->finalCostLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->finalCostLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->finalCostLabel->Location = System::Drawing::Point(4, 391);
			this->finalCostLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->finalCostLabel->Name = L"finalCostLabel";
			this->finalCostLabel->Size = System::Drawing::Size(228, 19);
			this->finalCostLabel->TabIndex = 18;
			this->finalCostLabel->Text = L"�������� ��������� ������:";
			// 
			// endRentDateLabel
			// 
			this->endRentDateLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->endRentDateLabel->AutoSize = true;
			this->endRentDateLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->endRentDateLabel->Location = System::Drawing::Point(8, 306);
			this->endRentDateLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->endRentDateLabel->Name = L"endRentDateLabel";
			this->endRentDateLabel->Size = System::Drawing::Size(148, 19);
			this->endRentDateLabel->TabIndex = 17;
			this->endRentDateLabel->Text = L"���� ����� ������:";
			// 
			// rentDaysLabel
			// 
			this->rentDaysLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->rentDaysLabel->AutoSize = true;
			this->rentDaysLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->rentDaysLabel->Location = System::Drawing::Point(8, 248);
			this->rentDaysLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->rentDaysLabel->Name = L"rentDaysLabel";
			this->rentDaysLabel->Size = System::Drawing::Size(189, 19);
			this->rentDaysLabel->TabIndex = 15;
			this->rentDaysLabel->Text = L"���������� ���� ������:";
			// 
			// rentDaysTextField
			// 
			this->rentDaysTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->rentDaysTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->rentDaysTextField->Location = System::Drawing::Point(10, 271);
			this->rentDaysTextField->Margin = System::Windows::Forms::Padding(6, 2, 2, 2);
			this->rentDaysTextField->Name = L"rentDaysTextField";
			this->rentDaysTextField->Size = System::Drawing::Size(179, 28);
			this->rentDaysTextField->TabIndex = 25;
			this->rentDaysTextField->TextChanged += gcnew System::EventHandler(this, &MainForm::rentDaysTextField_TextChanged);
			// 
			// startRentDateLabel
			// 
			this->startRentDateLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->startRentDateLabel->AutoSize = true;
			this->startRentDateLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->startRentDateLabel->Location = System::Drawing::Point(6, 191);
			this->startRentDateLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->startRentDateLabel->Name = L"startRentDateLabel";
			this->startRentDateLabel->Size = System::Drawing::Size(156, 19);
			this->startRentDateLabel->TabIndex = 13;
			this->startRentDateLabel->Text = L"���� ������ ������:";
			// 
			// startDateTextField
			// 
			this->startDateTextField->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->startDateTextField->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->startDateTextField->Location = System::Drawing::Point(10, 212);
			this->startDateTextField->Margin = System::Windows::Forms::Padding(6, 2, 2, 2);
			this->startDateTextField->Name = L"startDateTextField";
			this->startDateTextField->Size = System::Drawing::Size(181, 28);
			this->startDateTextField->TabIndex = 24;
			this->startDateTextField->TextChanged += gcnew System::EventHandler(this, &MainForm::startDateTextField_TextChanged);
			// 
			// fathLabel
			// 
			this->fathLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->fathLabel->AutoSize = true;
			this->fathLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->fathLabel->Location = System::Drawing::Point(9, 137);
			this->fathLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->fathLabel->Name = L"fathLabel";
			this->fathLabel->Size = System::Drawing::Size(79, 19);
			this->fathLabel->TabIndex = 11;
			this->fathLabel->Text = L"��������:";
			// 
			// lastNameLabel
			// 
			this->lastNameLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->lastNameLabel->AutoSize = true;
			this->lastNameLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->lastNameLabel->Location = System::Drawing::Point(9, 100);
			this->lastNameLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->lastNameLabel->Name = L"lastNameLabel";
			this->lastNameLabel->Size = System::Drawing::Size(77, 19);
			this->lastNameLabel->TabIndex = 9;
			this->lastNameLabel->Text = L"�������:";
			// 
			// firstNameLabel
			// 
			this->firstNameLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->firstNameLabel->AutoSize = true;
			this->firstNameLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->firstNameLabel->Location = System::Drawing::Point(9, 61);
			this->firstNameLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->firstNameLabel->Name = L"firstNameLabel";
			this->firstNameLabel->Size = System::Drawing::Size(42, 19);
			this->firstNameLabel->TabIndex = 7;
			this->firstNameLabel->Text = L"���:";
			// 
			// searchBox
			// 
			this->searchBox->Controls->Add(this->badsCountLabel);
			this->searchBox->Controls->Add(this->badsCountSelect);
			this->searchBox->Controls->Add(this->parkingCheckBox);
			this->searchBox->Controls->Add(this->minCostTextField);
			this->searchBox->Controls->Add(this->wifiCheckBox);
			this->searchBox->Controls->Add(this->roomsCountSelect);
			this->searchBox->Controls->Add(this->MinRoomsLabel);
			this->searchBox->Controls->Add(this->minCostLabel);
			this->searchBox->Controls->Add(this->minSqureTextField);
			this->searchBox->Controls->Add(this->searchBtn);
			this->searchBox->Controls->Add(this->minSquareLabel);
			this->searchBox->Controls->Add(this->adressLabel);
			this->searchBox->Controls->Add(this->adressTextField);
			this->searchBox->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->searchBox->Location = System::Drawing::Point(6, 31);
			this->searchBox->Margin = System::Windows::Forms::Padding(2);
			this->searchBox->Name = L"searchBox";
			this->searchBox->Padding = System::Windows::Forms::Padding(2);
			this->searchBox->Size = System::Drawing::Size(1266, 73);
			this->searchBox->TabIndex = 6;
			this->searchBox->TabStop = false;
			this->searchBox->Text = L"����� ��������";
			// 
			// badsCountLabel
			// 
			this->badsCountLabel->AutoSize = true;
			this->badsCountLabel->Location = System::Drawing::Point(582, 30);
			this->badsCountLabel->Name = L"badsCountLabel";
			this->badsCountLabel->Size = System::Drawing::Size(103, 16);
			this->badsCountLabel->TabIndex = 32;
			this->badsCountLabel->Text = L"���.��������:";
			// 
			// badsCountSelect
			// 
			this->badsCountSelect->FormattingEnabled = true;
			this->badsCountSelect->Items->AddRange(gcnew cli::array< System::Object^  >(10)
			{
				L"1", L"2", L"3", L"4", L"5", L"6", L"7",
					L"8", L"9", L"10"
			});
			this->badsCountSelect->Location = System::Drawing::Point(691, 27);
			this->badsCountSelect->Name = L"badsCountSelect";
			this->badsCountSelect->Size = System::Drawing::Size(39, 24);
			this->badsCountSelect->TabIndex = 3;
			// 
			// parkingCheckBox
			// 
			this->parkingCheckBox->AutoSize = true;
			this->parkingCheckBox->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->parkingCheckBox->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->parkingCheckBox->Location = System::Drawing::Point(827, 29);
			this->parkingCheckBox->Name = L"parkingCheckBox";
			this->parkingCheckBox->Size = System::Drawing::Size(99, 23);
			this->parkingCheckBox->TabIndex = 5;
			this->parkingCheckBox->Text = L"��������:";
			this->parkingCheckBox->UseVisualStyleBackColor = true;
			// 
			// minCostTextField
			// 
			this->minCostTextField->HidePromptOnLeave = true;
			this->minCostTextField->Location = System::Drawing::Point(1056, 27);
			this->minCostTextField->Margin = System::Windows::Forms::Padding(2);
			this->minCostTextField->Mask = L"99999999999999999999999";
			this->minCostTextField->Name = L"minCostTextField";
			this->minCostTextField->Size = System::Drawing::Size(97, 24);
			this->minCostTextField->TabIndex = 6;
			// 
			// wifiCheckBox
			// 
			this->wifiCheckBox->AutoSize = true;
			this->wifiCheckBox->CheckAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->wifiCheckBox->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->wifiCheckBox->Location = System::Drawing::Point(753, 28);
			this->wifiCheckBox->Name = L"wifiCheckBox";
			this->wifiCheckBox->Size = System::Drawing::Size(57, 25);
			this->wifiCheckBox->TabIndex = 4;
			this->wifiCheckBox->Text = L"wifi:";
			this->wifiCheckBox->UseVisualStyleBackColor = true;
			// 
			// roomsCountSelect
			// 
			this->roomsCountSelect->FormattingEnabled = true;
			this->roomsCountSelect->Items->AddRange(gcnew cli::array< System::Object^  >(10)
			{
				L"1", L"2", L"3", L"4", L"5", L"6", L"7",
					L"8", L"9", L"10"
			});
			this->roomsCountSelect->Location = System::Drawing::Point(541, 27);
			this->roomsCountSelect->Name = L"roomsCountSelect";
			this->roomsCountSelect->Size = System::Drawing::Size(35, 24);
			this->roomsCountSelect->TabIndex = 2;
			// 
			// MinRoomsLabel
			// 
			this->MinRoomsLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->MinRoomsLabel->AutoSize = true;
			this->MinRoomsLabel->Location = System::Drawing::Point(447, 31);
			this->MinRoomsLabel->Name = L"MinRoomsLabel";
			this->MinRoomsLabel->Size = System::Drawing::Size(88, 16);
			this->MinRoomsLabel->TabIndex = 6;
			this->MinRoomsLabel->Text = L"���.������:";
			// 
			// minCostLabel
			// 
			this->minCostLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->minCostLabel->AutoSize = true;
			this->minCostLabel->Location = System::Drawing::Point(943, 31);
			this->minCostLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->minCostLabel->Name = L"minCostLabel";
			this->minCostLabel->Size = System::Drawing::Size(109, 16);
			this->minCostLabel->TabIndex = 5;
			this->minCostLabel->Text = L"���.�����/���:";
			// 
			// minSqureTextField
			// 
			this->minSqureTextField->HidePromptOnLeave = true;
			this->minSqureTextField->Location = System::Drawing::Point(360, 27);
			this->minSqureTextField->Margin = System::Windows::Forms::Padding(2);
			this->minSqureTextField->Mask = L"999999999999999999999";
			this->minSqureTextField->Name = L"minSqureTextField";
			this->minSqureTextField->Size = System::Drawing::Size(78, 24);
			this->minSqureTextField->TabIndex = 1;
			// 
			// searchBtn
			// 
			this->searchBtn->Anchor = System::Windows::Forms::AnchorStyles::Right;
			this->searchBtn->AutoSize = true;
			this->searchBtn->BackColor = System::Drawing::Color::CornflowerBlue;
			this->searchBtn->FlatStyle = System::Windows::Forms::FlatStyle::Popup;
			this->searchBtn->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->searchBtn->ForeColor = System::Drawing::SystemColors::ActiveCaptionText;
			this->searchBtn->Location = System::Drawing::Point(1169, 22);
			this->searchBtn->Margin = System::Windows::Forms::Padding(2);
			this->searchBtn->Name = L"searchBtn";
			this->searchBtn->Size = System::Drawing::Size(83, 35);
			this->searchBtn->TabIndex = 7;
			this->searchBtn->Text = L"������";
			this->searchBtn->UseVisualStyleBackColor = false;
			this->searchBtn->Click += gcnew System::EventHandler(this, &MainForm::searchBtn_Click);
			// 
			// minSquareLabel
			// 
			this->minSquareLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->minSquareLabel->AutoSize = true;
			this->minSquareLabel->Location = System::Drawing::Point(252, 30);
			this->minSquareLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->minSquareLabel->Name = L"minSquareLabel";
			this->minSquareLabel->Size = System::Drawing::Size(104, 16);
			this->minSquareLabel->TabIndex = 3;
			this->minSquareLabel->Text = L"���.��������:";
			// 
			// adressLabel
			// 
			this->adressLabel->Anchor = System::Windows::Forms::AnchorStyles::None;
			this->adressLabel->Location = System::Drawing::Point(4, 31);
			this->adressLabel->Margin = System::Windows::Forms::Padding(2, 0, 2, 0);
			this->adressLabel->Name = L"adressLabel";
			this->adressLabel->Size = System::Drawing::Size(54, 16);
			this->adressLabel->TabIndex = 1;
			this->adressLabel->Text = L"�����:";
			// 
			// adressTextField
			// 
			this->adressTextField->Location = System::Drawing::Point(65, 27);
			this->adressTextField->Margin = System::Windows::Forms::Padding(2);
			this->adressTextField->Name = L"adressTextField";
			this->adressTextField->Size = System::Drawing::Size(183, 24);
			this->adressTextField->TabIndex = 0;
			// 
			// rommListBox
			// 
			this->rommListBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->rommListBox->Controls->Add(this->roomList);
			this->rommListBox->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->rommListBox->Location = System::Drawing::Point(6, 107);
			this->rommListBox->Margin = System::Windows::Forms::Padding(2);
			this->rommListBox->Name = L"rommListBox";
			this->rommListBox->Padding = System::Windows::Forms::Padding(2);
			this->rommListBox->Size = System::Drawing::Size(1004, 492);
			this->rommListBox->TabIndex = 5;
			this->rommListBox->TabStop = false;
			this->rommListBox->Text = L"������ �������";
			// 
			// roomList
			// 
			this->roomList->Activation = System::Windows::Forms::ItemActivation::OneClick;
			this->roomList->Columns->AddRange(gcnew cli::array< System::Windows::Forms::ColumnHeader^  >(8)
			{
				this->id, this->address,
					this->sqare, this->roomsCount, this->badsCount, this->wifi, this->parking, this->cost
			});
			this->roomList->Dock = System::Windows::Forms::DockStyle::Fill;
			this->roomList->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->roomList->FullRowSelect = true;
			this->roomList->GridLines = true;
			this->roomList->HeaderStyle = System::Windows::Forms::ColumnHeaderStyle::Nonclickable;
			this->roomList->HideSelection = false;
			this->roomList->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->roomList->Location = System::Drawing::Point(2, 23);
			this->roomList->Margin = System::Windows::Forms::Padding(2);
			this->roomList->MultiSelect = false;
			this->roomList->Name = L"roomList";
			this->roomList->Size = System::Drawing::Size(1000, 467);
			this->roomList->TabIndex = 0;
			this->roomList->UseCompatibleStateImageBehavior = false;
			this->roomList->View = System::Windows::Forms::View::Details;
			this->roomList->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::roomList_SelectedIndexChanged);
			// 
			// id
			// 
			this->id->Text = L"id";
			this->id->Width = 27;
			// 
			// address
			// 
			this->address->Text = L"�����";
			this->address->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->address->Width = 370;
			// 
			// sqare
			// 
			this->sqare->Text = L"������� (�^3)";
			this->sqare->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->sqare->Width = 142;
			// 
			// roomsCount
			// 
			this->roomsCount->Text = L"������";
			this->roomsCount->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->roomsCount->Width = 66;
			// 
			// badsCount
			// 
			this->badsCount->Text = L"��������";
			this->badsCount->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->badsCount->Width = 87;
			// 
			// wifi
			// 
			this->wifi->Text = L"wifi";
			this->wifi->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->wifi->Width = 62;
			// 
			// parking
			// 
			this->parking->Text = L"��������";
			this->parking->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->parking->Width = 80;
			// 
			// cost
			// 
			this->cost->Text = L"��������� (P./���.)";
			this->cost->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->cost->Width = 162;
			// 
			// showHistoryBtn
			// 
			this->showHistoryBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->showHistoryBtn->AutoSize = true;
			this->showHistoryBtn->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->showHistoryBtn->Location = System::Drawing::Point(-1, -1);
			this->showHistoryBtn->Name = L"showHistoryBtn";
			this->showHistoryBtn->Size = System::Drawing::Size(125, 26);
			this->showHistoryBtn->TabIndex = 10;
			this->showHistoryBtn->Text = L"��������� ������";
			this->showHistoryBtn->UseVisualStyleBackColor = true;
			this->showHistoryBtn->Click += gcnew System::EventHandler(this, &MainForm::dealHistoryBtn_Click);
			// 
			// adminBtn
			// 
			this->adminBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->adminBtn->AutoSize = true;
			this->adminBtn->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->adminBtn->Location = System::Drawing::Point(123, -1);
			this->adminBtn->Name = L"adminBtn";
			this->adminBtn->Size = System::Drawing::Size(187, 26);
			this->adminBtn->TabIndex = 11;
			this->adminBtn->Text = L"���������� �������������";
			this->adminBtn->UseVisualStyleBackColor = true;
			this->adminBtn->Click += gcnew System::EventHandler(this, &MainForm::adminBtn_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(1372, 604);
			this->Controls->Add(this->adminBtn);
			this->Controls->Add(this->showHistoryBtn);
			this->Controls->Add(this->rentProcessBox);
			this->Controls->Add(this->searchBox);
			this->Controls->Add(this->rommListBox);
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Tai Le", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Margin = System::Windows::Forms::Padding(2);
			this->Name = L"Form1";
			this->Text = L"������ �������";
			this->rentProcessBox->ResumeLayout(false);
			this->rentProcessBox->PerformLayout();
			this->searchBox->ResumeLayout(false);
			this->searchBox->PerformLayout();
			this->rommListBox->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		System::Void adminBtn_Click(System::Object^ sender, System::EventArgs^ e)
		{
			adminForm = gcnew AdminForm();
			string history = this->userInterface->getDealsHistory();
			if(history != "")
			{
				adminForm->Visible = true;
				adminForm->onRoomAddedBtn->Click += gcnew System::EventHandler(this, &MainForm::onRoomAdded);
				adminForm->coreTextBox->Text = ConvertUtills::stringToTextBoxString(history);
			}
			else
			{
				displayWarningMessage("���� ������� ������ �������� ��� ���������� � ������ ����������, ����������� ���� � ���� ���������� � �����������!");
			}			
		}
		bool addNewRoom()
		{
			return this->userInterface->addNewRoom(
				this->adminForm->adressReactField->value,
				stoi(this->adminForm->houceNumberReactField->value),
				stoi(this->adminForm->squareReactField->value),
				stoi(this->adminForm->moneyCostReactField->value),
				this->adminForm->wifiCheckBox->Checked,
				stoi(this->adminForm->roomsCountReactField->value),
				stoi(this->adminForm->badsCountReactField->value),
				this->adminForm->parkingCheckBox->Checked
			);
		}
		System::Void onRoomAdded(System::Object^ sender, System::EventArgs^ e)
		{
			bool addRes = addNewRoom();
			if(addRes)
			{
				displaySuccessMessage("�������� ���������!");

				this->adminForm->adressTextField->Text = "";
				this->adminForm->minSqureTextField->Text = "";
				this->adminForm->roomsCountSelect->Text = "";
				this->adminForm->badsCountSelect->Text = "";
				this->adminForm->minCostTextField->Text = "";
				this->adminForm->houceNumberTextField->Text = "";
			}
			else
			{
				displayErrorMessage("��� �� ����� �� ���!");
			}
			this->userInterface->getAllRooms();
			updateRoomList();
		}
		System::Void dealHistoryBtn_Click(System::Object^ sender, System::EventArgs^ e)
		{
			LastDealForm^ dealsHistoryForm = gcnew LastDealForm();
			string history = this->userInterface->getLastDealHistory();
			if(history != "")
			{
				dealsHistoryForm->Visible = true;
				dealsHistoryForm->updateCoreText(ConvertUtills::stringToTextBoxString(history));
			}
			else
			{
				displayWarningMessage("���� ������� ������ �������� ��� ���������� � ������ ����������, ����������� ���� � ���� ���������� � �����������!");
			}
		}
		System::Void searchBtn_Click(System::Object^ sender, System::EventArgs^ e)
		{
			string adress = ConvertUtills::textBoxStringToString(this->adressTextField->Text);
			string squareStr = ConvertUtills::textBoxStringToString(this->minSqureTextField->Text);
			string rooms = ConvertUtills::textBoxStringToString(this->roomsCountSelect->Text);
			string costStr = ConvertUtills::textBoxStringToString(this->minCostTextField->Text);
			string bads = ConvertUtills::textBoxStringToString(this->badsCountSelect->Text);
			bool wifi = this->wifiCheckBox->Checked;
			bool parking = this->parkingCheckBox->Checked;

			bool costIsValid = ValidationUtills::isPositiveNumber(costStr);
			bool squareIsValid = ValidationUtills::isPositiveNumber(squareStr);
			bool roomCountIsValid = ValidationUtills::isPositiveNumber(rooms);
			bool badsCountIsValid = ValidationUtills::isPositiveNumber(bads);


			int square = -1;
			int cost = -1;
			int roomsCount = -1;
			int badsCount = -1;

			if(squareIsValid)
			{
				square = stoi(squareStr);
			}
			if(roomCountIsValid)
			{
				roomsCount = stoi(rooms);
			}
			if(badsCountIsValid)
			{
				badsCount = stoi(bads);
			}
			if(costIsValid)
			{
				cost = stoi(costStr);
			}
			userInterface->searchRoom(false, adress, roomsCount, square, badsCount, cost, wifi, parking);
			updateRoomList();
		}
		System::Void rentBtn_Click(System::Object^ sender, System::EventArgs^ e)
		{
			if(this->firstNameReactField->valid
				&& this->lastNameReactField->valid
				&& this->fathReactField->valid
				&& this->rentDaysReactField->valid
				&& this->startRentDateReactField->valid
				&& this->chosenRoomIndexReactField->valid)
			{
				Renter* renter = new Renter(this->firstNameReactField->value, this->lastNameReactField->value, this->fathReactField->value);
				int days = stoi(this->rentDaysReactField->value);
				tm* startDate = ConvertUtills::stringToDate(this->startRentDateReactField->value);
				bool rentSuccess = userInterface->takeRoom(renter, startDate, days);
				if(rentSuccess)
				{
					displaySuccessMessage("�������� ����������!");
				}
				else
				{
					displayErrorMessage("��� �� ����� �� ���!");
				}
				this->chosenRoomIndexReactField->update(to_string(this->userInterface->selectedRoomIndex));
				this->rentDaysTextField->Text = "";
				this->firstNameTextField->Text = "";
				this->lastNameTextField->Text = "";
				this->fathTextField->Text = "";

				updateRoomList();
				setFinalCost();
				updateButtonState();
			}
			else
			{
				string errors = "";
				if(!this->firstNameReactField->valid)
				{
					errors += this->firstNameReactField->invalidMessage + "\n\n";
				}
				if(!this->lastNameReactField->valid)
				{
					errors += this->lastNameReactField->invalidMessage + "\n\n";
				}
				if(!this->fathReactField->valid)
				{
					errors += this->fathReactField->invalidMessage + "\n\n";
				}
				if(!this->rentDaysReactField->valid)
				{
					errors += this->rentDaysReactField->invalidMessage + "\n\n";
				}
				if(!this->startRentDateReactField->valid)
				{
					errors += this->startRentDateReactField->invalidMessage + "\n\n";
				}
				if(!this->chosenRoomIndexReactField->valid)
				{
					errors += this->chosenRoomIndexReactField->invalidMessage + "\n\n";
				}
				displayErrorMessage(errors);
			}
		}
		System::Void roomList_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e)
		{
			if(this->roomList->FocusedItem == nullptr || this->roomList->SelectedItems->Count == 0)
			{
				this->chosenRoomIndexReactField->update("-1");
			}
			else
			{
				this->chosenRoomIndexReactField->update(to_string(this->roomList->FocusedItem->Index + 1));
			}
			this->userInterface->selectedRoomIndex = stoi(this->chosenRoomIndexReactField->value) - 1;
			setFinalCost();
			updateButtonState();
		}
		System::Void startDateTextField_TextChanged(System::Object^ sender, System::EventArgs^ e)
		{
			this->startRentDateReactField->update(ConvertUtills::textBoxStringToString(this->startDateTextField->Text));
			onDateChanged();
			updateButtonState();
		}
		System::Void rentDaysTextField_TextChanged(System::Object^ sender, System::EventArgs^ e)
		{
			this->rentDaysReactField->update(ConvertUtills::textBoxStringToString(this->rentDaysTextField->Text));
			onDateChanged();
			updateButtonState();
		}
		System::Void firstNameTextField_TextChanged(System::Object^ sender, System::EventArgs^ e)
		{
			this->firstNameReactField->update(ConvertUtills::textBoxStringToString(this->firstNameTextField->Text));
			updateButtonState();
		}
		System::Void lastNameTextField_TextChanged(System::Object^ sender, System::EventArgs^ e)
		{
			this->lastNameReactField->update(ConvertUtills::textBoxStringToString(this->lastNameTextField->Text));
			updateButtonState();
		}
		System::Void fathTextField_TextChanged(System::Object^ sender, System::EventArgs^ e)
		{
			this->fathReactField->update(ConvertUtills::textBoxStringToString(this->fathTextField->Text));
			updateButtonState();
		}

		void setRentEndDate()
		{
			this->endDateTextField->ForeColor = Color::Black;
			this->endDateTextField->Text = ConvertUtills::stringToTextBoxString(this->endRentDateReactField->value);
		}
		void setFinalCost()
		{
			if(this->rentDaysReactField->valid
				&& this->chosenRoomIndexReactField->valid
				&& this->startRentDateReactField->valid)
			{
				double cost = this->userInterface->getCurrentRoomCost(ConvertUtills::stringToDate(this->startRentDateReactField->value), stoi(this->rentDaysReactField->value));
				this->finalCostReactField->update(to_string(cost));
				string costStr = this->finalCostReactField->value;

				this->finalCostTextField->ForeColor = Color::Black;
				this->finalCostTextField->Text = ConvertUtills::stringToTextBoxString(costStr.substr(0, costStr.find_first_of('.')) + " P.");
			}
			else if(!this->chosenRoomIndexReactField->valid)
			{
				printReadonlyRedText(this->finalCostTextField, this->chosenRoomIndexReactField->invalidMessage);
			}
			else if(!this->rentDaysReactField->valid)
			{
				printReadonlyRedText(this->finalCostTextField, this->rentDaysReactField->invalidMessage);
			}
			else if(!this->startRentDateReactField->valid)
			{
				printReadonlyRedText(this->finalCostTextField, this->startRentDateReactField->invalidMessage);
			}
			else
			{
				printReadonlyRedText(this->finalCostTextField, "�������� �����-�� ������!");
			}
		}
		void onDateChanged()
		{
			if(this->rentDaysReactField->valid && this->startRentDateReactField->valid)
			{
				int rentDays = stoi(this->rentDaysReactField->value);


				this->userInterface->rentDays = rentDays;
				this->userInterface->startDate = ConvertUtills::stringToDate(this->startRentDateReactField->value);

				tm* endDate = ConvertUtills::stringToDate(this->startRentDateReactField->value);

				time_t timeNow = time(0);
				endDate->tm_mday += rentDays;
				timeNow = mktime(endDate);
				localtime_s(endDate, &timeNow);

				this->userInterface->endDate = endDate;
				this->endRentDateReactField->update(ConvertUtills::dateToString(endDate));

				setRentEndDate();
			}
			else if(!this->rentDaysReactField->valid)
			{
				printReadonlyRedText(this->endDateTextField, this->rentDaysReactField->invalidMessage);
			}
			else if(!this->startRentDateReactField->valid)
			{
				printReadonlyRedText(this->endDateTextField, this->startRentDateReactField->invalidMessage);
			}
			else
			{
				printReadonlyRedText(this->endDateTextField, "�������� �����-�� ������!");
			}
			setFinalCost();
			updateButtonState();

		}
		void updateButtonState()
		{
			if(this->chosenRoomIndexReactField->valid
				&& this->startRentDateReactField->valid
				&& this->rentDaysReactField->valid
				&& this->firstNameReactField->valid
				&& this->lastNameReactField->valid
				&& this->fathReactField->valid)
			{
				this->rentBtn->BackColor = System::Drawing::Color::LightGreen;
			}
			else
			{
				this->rentBtn->BackColor = System::Drawing::Color::Yellow;
			}
		}
		void printReadonlyRedText(RichTextBox^ textBox, string str)
		{
			textBox->ReadOnly = false;
			textBox->ForeColor = Color::Red;
			textBox->ReadOnly = true;
			textBox->Text = ConvertUtills::stringToTextBoxString(str);
		}
		void updateRoomList()
		{
			this->roomList->Items->Clear();
			for(Room* room : userInterface->currentRoomList)
			{
				String^ id = ConvertUtills::stringToTextBoxString(to_string(room->id));
				String^ adress = ConvertUtills::stringToTextBoxString(room->address) + ConvertUtills::stringToTextBoxString(" " + to_string(room->houseNumber));
				String^ roomCount = ConvertUtills::stringToTextBoxString(to_string(room->roomsCount));
				String^ wifi = room->wifi ? "��" : "���";
				String^ square = ConvertUtills::stringToTextBoxString(to_string(room->square));
				String^ cost = ConvertUtills::stringToTextBoxString(to_string(room->moneyPerMonth));
				String^ bads = ConvertUtills::stringToTextBoxString(to_string(room->badsCount));
				String^ parking = room->parking ? "��" : "���";
				ListViewItem^ roomItem = gcnew ListViewItem();
				roomItem->Text = id;
				roomItem->SubItems->Add(adress);
				roomItem->SubItems->Add(square);
				roomItem->SubItems->Add(roomCount);
				roomItem->SubItems->Add(bads);
				roomItem->SubItems->Add(wifi);
				roomItem->SubItems->Add(parking);
				roomItem->SubItems->Add(cost);
				this->roomList->Items->Add(roomItem);
			}
		}
		void displaySuccessMessage(string msg)
		{
			System::Windows::Forms::DialogResult result = MessageBox::Show(
				ConvertUtills::stringToTextBoxString(msg),
				"�����!",
				MessageBoxButtons::OK,
				MessageBoxIcon::Asterisk
			);
		}
		void displayWarningMessage(string msg)
		{
			System::Windows::Forms::DialogResult result = MessageBox::Show(
				ConvertUtills::stringToTextBoxString(msg),
				"��������!",
				MessageBoxButtons::OK,
				MessageBoxIcon::Warning
			);
		}
		void displayErrorMessage(string msg)
		{
			System::Windows::Forms::DialogResult result = MessageBox::Show(
				ConvertUtills::stringToTextBoxString(msg),
				"������!",
				MessageBoxButtons::OK,
				MessageBoxIcon::Error
			);
		}
	};
}
