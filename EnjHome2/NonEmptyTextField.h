#pragma once
#include "ReactiveComponent.h"
#include "../EnjHom2Lib/ConvertUtills.h"
#include "../EnjHom2Lib/ValidationUtills.h"

class NonEmptyTextField: public ReactiveComponent
{
public:

	NonEmptyTextField(string defaultInvalidMessage) : ReactiveComponent(defaultInvalidMessage)
	{

	}
	void validate()
	{
		this->invalidMessage = this->defaultInvalidMessage;
		this->valid = false;
		if(this->value.size() > 0)
		{
			this->valid = true;
			this->invalidMessage = "";
		}
	}

};


