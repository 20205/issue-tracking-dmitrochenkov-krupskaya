#include "MainForm.h"
using namespace GUI;

int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, int)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew MainForm);
}

