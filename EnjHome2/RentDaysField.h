#pragma once
#include "ReactiveComponent.h"
#include "../EnjHom2Lib/ConvertUtills.h"
#include "../EnjHom2Lib/ValidationUtills.h"

class RentDaysField: public ReactiveComponent
{

public: 

	RentDaysField(string defaultInvalidMessage) : ReactiveComponent(defaultInvalidMessage)
	{

	}
	void validate()
	{
		this->valid = false;
		this->invalidMessage = this->defaultInvalidMessage;
		int days = -1;
		bool daysIsPosNumber = ValidationUtills::isPositiveNumber(this->value);
		if(daysIsPosNumber && stoi(this->value) > 0)
		{
			days = stoi(this->value);
			if(days > 365)
			{
				this->valid = false;
				this->invalidMessage = "������� ������ ����!";
			}
			else if(stoi(this->value) <= 0)
			{
				this->valid = false;
				this->invalidMessage = "�������� ���� ������!";
			} 
			else 
			{
				this->valid = true;
				this->invalidMessage = "";
			}
		}

	}

};

