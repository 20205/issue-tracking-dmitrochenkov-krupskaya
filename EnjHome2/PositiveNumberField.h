#pragma once
#include "ReactiveComponent.h"
#include "../EnjHom2Lib/ConvertUtills.h"
#include "../EnjHom2Lib/ValidationUtills.h"

class PositiveNumberField: public ReactiveComponent
{
public:

	PositiveNumberField(string defaultInvalidMessage) : ReactiveComponent(defaultInvalidMessage)
	{

	}
	void validate()
	{
		this->invalidMessage = this->defaultInvalidMessage;
		this->valid = false;
		if(ValidationUtills::isPositiveNumber(this->value) && stoi(this->value) >= 0)
		{
			this->valid = true;
			this->invalidMessage = "";
		}
	}

};


