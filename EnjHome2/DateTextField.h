#pragma once
#include "ReactiveComponent.h"
#include "../EnjHom2Lib/ConvertUtills.h"
#include "../EnjHom2Lib/ValidationUtills.h"

class DateTextField: public ReactiveComponent
{
public:

	DateTextField(string defaultInvalidMessage) : ReactiveComponent(defaultInvalidMessage)
	{

	}
	void validate()
	{
		this->valid = false;
		this->invalidMessage = this->defaultInvalidMessage;
		bool dateFormatIsValid = ValidationUtills::dateFormatIsCorrect(this->value);
		bool dateIsCorrect = ValidationUtills::dateIsInCorrectRange(ConvertUtills::stringToDate(this->value));
		if(dateFormatIsValid && dateIsCorrect)
		{
			this->valid = true;
			this->invalidMessage = "";
		}
		else if(!dateFormatIsValid)
		{
			this->invalidMessage = "�������� ����� ����!";
		}
		else if(!dateIsCorrect)
		{
			this->invalidMessage = "������� ������ ����!";
		}
	}

};

