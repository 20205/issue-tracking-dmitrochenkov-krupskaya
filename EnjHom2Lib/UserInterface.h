#pragma once
#include "Room.h"
#include <vector>
#include "RoomBase.h"
#include "RentController.h"

class UserInterface
{

private: 


	RoomBase* roomBase;
	RentController* rentController;

public:

	int rentDays = -1;
	tm* startDate = NULL;
	tm* endDate = NULL;
	vector<Room*> currentRoomList = vector<Room*>();
	int selectedRoomIndex = -1;

	UserInterface(RoomBase* roomBase, RentController* rentController);

	~UserInterface()
	{
		delete(startDate);
		delete(endDate);
		delete(roomBase);
		delete(rentController);
	}


	/**
	* Add new room to base
	*
	* @param address new room adress
	* @param houseNumber new room houce number
	* @param square new room square
	* @param moneyPerMonth new room money per month
	* @param wifi new room is wifi present
	* @param roomCount new room rroms count
	* @param badsCount new room bads count
	* @param parking new room is parking present
	*
	* @return is room saved
	**/
	bool addNewRoom(string address, int houseNumber, int square, int moneyPerMonth, bool wifi, int roomCount, int badsCount, bool parking);

	/**
	* Get last deal data as string
	**/
	string getLastDealHistory();

	/**
	* Get add deals data as string
	**/
	string getDealsHistory();

	/**
	* Calc room cost for some days
	*
	* @param rentStartDate rent start date
	* @param rentDays day count to take room
	*
	* @return romm cost for rented days and rent start date
	**/
	double getCurrentRoomCost(tm* startDate, int rentDays);

	/**
	* Method which colling when "search" button clicked
	*
	* @param rented Is searching room rented
	* @param adress Searching room adress
	* @param roomsCount Bottom border of rooms cout 
	* @param money Bottom border of room month cost
	* @param square Bottom border of room square
	* @param badsCount bads count in room
	* @param wifi is wifi present
	* @param parking is parking present
	*
	* @return Is search request valid and executed
	**/
	bool searchRoom(bool rented, string address, int roomsCount, int square, int badsCount, int money, bool wifi, bool parking);

	/**
	* Load rooms to current room list
	*
	* @return Is loading done successfuly
	**/
	bool getAllRooms();

	/**
	* Method which colling when "rent" button clicked
	*
	* @param rentStartDate rent start date
	* @param rentDays day count to take room
	*
	* @return Is room successfuly rented
	**/
	bool takeRoom(Renter* renter, tm* startDate, int rentDays);


};

