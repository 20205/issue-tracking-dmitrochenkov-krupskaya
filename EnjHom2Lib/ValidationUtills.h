#pragma once
#include <string>
#include <time.h>
#include "ConvertUtills.h"
using namespace std;
using namespace System;

class ValidationUtills
{

public:

	/**
	* Check is string can be converted to number
	*
	* @param str string to check
	* @return Is string can be converted to number
	**/
	static bool isNumber(string str);

	/**
	* Check is string can be converted to positive number 
	*
	* @param str string to check
	* @return Is string can be converted to positive number
	**/
	static bool isPositiveNumber(string str);

	/**
	* Check is string not empty
	*
	* @param str string to check
	* @return Is string empty
	**/
	static bool notEmptyString(string str);

	/**
	* Check is date correct: 
	*
	*	days and month count is correct,
	*	yaer diff to current year less then 3,
	*	date is current or later,
	*
	* @param date date to check
	* @return Is date correct
	**/
	static bool dateIsInCorrectRange(tm* date);

	/**
	* Check is date correct:
	*
	*	date format is DD.MM.YYYY,
	*	days and month count is correct,
	*	yaer diff to current year less then 3,
	*	date is current or later	
	*
	* @param str string to check
	* @return Is string correct date
	**/
	static bool dateFormatIsCorrect(string str);

};

