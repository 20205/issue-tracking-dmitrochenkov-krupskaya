#pragma once
#include <string>
#include <windows.h>
#include <iostream>
#include "Renter.h"
#include <ctime>
#include <time.h>
using namespace std;

class Room
{

public:

	int id;
	bool wifi = false;
	int roomsCount = 0;
	int badsCount = 0;
	bool parking = false;
	string address = "";
	int houseNumber = -1;
	int moneyPerMonth = -1;
	Renter* renter = NULL;
	int square = -1;
	bool isRented = false;
	tm rentStartDate = {0};
	tm rentEndDate = {0};
	int rentedDays = -1;

	Room(string address, int houseNumber, int square, int moneyPerMonth, bool wifi, int roomCount, int badsCount, bool parking)
	{
		this->id = rand() % 10000;
		this->wifi = wifi;
		this->address = address;
		this->roomsCount = roomCount;
		this->houseNumber = houseNumber;
		this->square = square;
		this->moneyPerMonth = moneyPerMonth;
		this->badsCount = badsCount;
		this->parking = parking;
	}

	Room() {}

	/**
	* Turn room to rented state
	*
	* @param renter New room renter
	* @param rentedDays Count of rent days
	**/
	void startRent(Renter* renter, tm* rentStartDate, int rentedDays);

	/**
	* Turn room to not rented state
	**/
	void endRent();

	/**
	* Save this Room to file
	*
	* @param ofStream File stream to save this Room
	**/
	void serealize(ostream& ofStream);

	/**
	* Load next Room from file
	*
	* @param ofStream File stream to load next Room
	*
	* @return Room instance - Loaded Renter
	**/
	Room* deserealize(istream& ofStream);

	/**
	* Present core Room data as text
	*
	* @return core Room data as text
	**/
	string getTextCoreData();

};

