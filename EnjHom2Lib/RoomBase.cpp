#include "RoomBase.h"


using namespace std;

RoomBase::RoomBase()
{
	vector<Room*> allRooms = findAll();
	vector<Room*> roomsToUpdate = vector<Room*>();
	for(int i = 0; i < allRooms.size(); i++)
	{
		Room* currentRoom = new Room();
		currentRoom = allRooms [i];
		time_t timeNow = time(0);
		time_t rentEndDate = mktime(&currentRoom->rentEndDate);
		if(rentEndDate != -1 && timeNow > rentEndDate)
		{
			currentRoom->isRented = false;
			currentRoom->renter = new Renter("", "", "");
			currentRoom->rentedDays = 0;
			currentRoom->rentStartDate = { 0 };
			currentRoom->rentEndDate = { 0 };
			roomsToUpdate.push_back(currentRoom);
		}
	}
	for(int i = 0; i < roomsToUpdate.size(); i++)
	{
		Room* currentRoom = new Room();
		currentRoom = roomsToUpdate [i];
		fingByIdAndUpdate(currentRoom->id, currentRoom);
	}
}

bool RoomBase::saveNewRoom(Room* room)
{
	try
	{
		ofstream fileOfstream(baseFilename, ios::binary | ios::app);
		room->serealize(fileOfstream);
		fileOfstream.close();
		return true;
	}
	catch(...)
	{
		return false;
	}
}

bool RoomBase::fingByIdAndUpdate(int id, Room* room)
{
	try
	{
		vector<Room*> allRooms = findAll();
		vector<Room*> resultRooms = vector<Room*>();
		Room* currentRoom;
		for(int i = 0; i < allRooms.size(); i++)
		{
			currentRoom = allRooms [i];
			if(currentRoom->id != id)
			{
				resultRooms.push_back(currentRoom);
			}
			else
			{
				resultRooms.push_back(room);
			}
		}
		remove(baseFilename.c_str());

		ofstream fileOfstream(baseFilename, ios::binary);
		for(int i = 0; i < resultRooms.size(); i++)
		{
			currentRoom = resultRooms [i];
			currentRoom->serealize(fileOfstream);
		}
		fileOfstream.close();
		return true;
	}
	catch(...)
	{
		return false;
	}
}

vector<Room*> RoomBase::findAll()
{
	try
	{
		ifstream inputFileStream(this->baseFilename, ios::binary);

		inputFileStream.seekg(0, ios::end);
		int size = (int) inputFileStream.tellg();
		inputFileStream.seekg(0, ios::beg);

		vector<Room*> resVector = vector<Room*>();

		while(inputFileStream.tellg() < size)
		{
			Room* roomBuffer = new Room();
			roomBuffer = roomBuffer->deserealize(inputFileStream);
			resVector.push_back(roomBuffer);
		}
		inputFileStream.close();

		return resVector;
	}
	catch(...)
	{
		return vector<Room*>();
	}
}

vector<Room*> RoomBase::findByConditions(bool rented, string address, int roomsCount, int square, int badsCount, int money, bool wifi, bool parking)
{
	vector<Room*> allRooms = findAll();
	vector<Room*> filteredRooms = vector<Room*>();
	Room* currentRoom;
	for(int i = 0; i < allRooms.size(); i++)
	{
		currentRoom = allRooms [i];
		if(currentRoom->isRented == rented
			&& (currentRoom->address == address || address == "")
			&& (currentRoom->moneyPerMonth >= money || money == -1)
			&& (currentRoom->square >= square || square == -1)
			&& (currentRoom->badsCount >= badsCount || badsCount == -1)
			&& (currentRoom->roomsCount >= roomsCount || roomsCount == -1)
			&& (currentRoom->wifi == wifi || !wifi)
			&& (currentRoom->parking == parking || !parking))
			filteredRooms.push_back(currentRoom);
	}
	return filteredRooms;
}

vector<Room*> RoomBase::findByRentedSortedByAddress(bool rented)
{
	vector<Room*> sortedRooms = findAll();
	vector<Room*> resultRooms = vector<Room*>();
	sort(sortedRooms.begin(), sortedRooms.end(), [](Room* room, Room* room1)
		{
			return room->address < room1->address;
		});
	Room* currentRoom;
	for(int i = 0; i < sortedRooms.size(); i++)
	{
		currentRoom = sortedRooms [i];
		if(currentRoom->isRented == rented)
		{
			resultRooms.push_back(currentRoom);
		}
	}
	return resultRooms;
}

