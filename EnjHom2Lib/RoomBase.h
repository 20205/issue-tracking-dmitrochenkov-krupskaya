#pragma once
#include "Room.h"
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <regex>

class RoomBase
{


public:

	string baseFilename = "V4__roomBase.dat";

	RoomBase();

	/**
	* Save new room to file
	*
	* @param room new room to save
	* @return is save was success
	**/
	bool saveNewRoom(Room* room);

	/**
	* Find room from base by ID and replace it 
	*
	* @param id ID of searched Room
	* @param room Room for replace
	*
	* @return Is room replaced successfuly
	**/
	bool fingByIdAndUpdate(int id, Room* room);

	/**
	* Find all rooms from base
	*
	* @return Vector<Room> with all romms from base
	**/
	vector<Room*> findAll();

	/**
	* Find rooms by conditions
	*
	* @param rented Is searching room rented
	* @param adress Searching room adress
	* @param roomsCount Bottom border of rooms count
	* @param money Bottom border of room month cost
	* @param square Bottom border of room square
	* @param badsCount bads count in room
	* @param wifi is wifi present
	* @param parking is parking present
	*
	* @return Vector<Room> with rooms satisfying conditions
	**/
	vector<Room*> findByConditions(bool rented, string adress, int roomsCount, int square, int badsCount, int money, bool wifi, bool parking);

	/**
	* Find all rooms by Romm.rented condition sorted by Room.adress
	*
	* @param rented Is searcing rooms rented
	*
	* @return Vector<Room> with Romm.rented == rented and sorted by adress
	**/
	vector<Room*> findByRentedSortedByAddress(bool rented);

};

