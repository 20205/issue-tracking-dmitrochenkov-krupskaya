#include "UserInterface.h"


UserInterface::UserInterface(RoomBase* roomBase, RentController* rentController)
{
	this->roomBase = roomBase;
	this->rentController = rentController;
}

bool UserInterface::addNewRoom(string address, int houseNumber, int square, int moneyPerMonth, bool wifi, int roomCount, int badsCount, bool parking)
{
	Room* newRoom = new Room(address, houseNumber, square, moneyPerMonth, wifi, roomCount, badsCount, parking);
	return this->roomBase->saveNewRoom(newRoom);
}

string UserInterface::getLastDealHistory()
{
	return this->rentController->getLastDealHistory();
}

string UserInterface::getDealsHistory()
{
	return this->rentController->getDealsHistory();
}

double UserInterface::getCurrentRoomCost(tm* startDate, int rentDays)
{
	Room* selectedRoom = this->currentRoomList [this->selectedRoomIndex];
	double payForDay = selectedRoom->moneyPerMonth / 30;

	time_t timeNow = time(0);
	time_t startDateTime = mktime(startDate);

	double timeDiff = difftime(startDateTime, timeNow);
	int daysFromNowToStartDate = ceil(timeDiff / 86400);

	double cost = payForDay * rentDays + (daysFromNowToStartDate * payForDay / 100 * 10);
	return cost;
}

bool UserInterface::searchRoom(bool rented, string address, int roomsCount, int square, int badsCount, int money, bool wifi, bool parking)
{
	try
	{
		this->currentRoomList = this->roomBase->findByConditions(rented, address, roomsCount, square, badsCount, money, wifi, parking);
		return true;
	}
	catch(...)
	{
		return false;
	}
}

bool UserInterface::getAllRooms()
{
	try
	{
		this->currentRoomList = this->roomBase->findByRentedSortedByAddress(false);
		return true;
	}
	catch(...)
	{
		return false;
	}
}

bool UserInterface::takeRoom(Renter* renter, tm* startDate, int rentDays)
{
	try
	{
		Room* selectedRoom = this->currentRoomList [this->selectedRoomIndex];
		double currentRoomCost = this->getCurrentRoomCost(startDate, rentDays);
		bool payResult = this->rentController->payForRent(currentRoomCost, renter, selectedRoom, startDate, rentDays);
		if(payResult)
		{
			this->roomBase->fingByIdAndUpdate(selectedRoom->id, selectedRoom);
			this->currentRoomList.erase(this->currentRoomList.begin() + this->selectedRoomIndex);
			this->selectedRoomIndex = -1;
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(...)
	{
		return false;
	}
}

