#pragma once
#include "Room.h"
#include "RoomBase.h"
using namespace std;

class RentController
{

public:

	/**
	* Make rent deal
	*
	* @param money Money provided by renter
	* @param room Room to rent
	* @param rentStartDate rent start date
	* @param rentDays day count to take room
	*
	* @return Is deal completed successfuly
	**/
	bool payForRent(double money, Renter* renter, Room* room, tm* rentStartDate, int rentDays);

	/**
	* Get last deal data as string
	**/
	string getLastDealHistory();

	/**
	* Get add deals data as string
	**/
	string getDealsHistory();

	double getMoney();

private:

	/**
	* Write room rent info in text file
	*
	* @param money money payed for rent
	* @param renter room renter
	* @param room rented room
	* 
	* @return is log was successful
	**/
	bool logTransaction(double money, Renter* renter, Room* room);

	string getLinesFromFile(string filename);

	double currentMoney = 0;
	string transactionLogsFilename = "renersLogs.txt";
	string lastLogFilename = "lastRent.txt";


};

