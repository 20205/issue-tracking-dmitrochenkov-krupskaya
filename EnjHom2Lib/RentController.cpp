#include "RentController.h"
#include <string>
#include <fstream>
#include "ConvertUtills.h"
#include <corecrt_io.h>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>

using namespace System::IO;

bool RentController::payForRent(double money, Renter* renter, Room* room, tm* rentStartDate, int rentDays)
{
	try
	{
		bool result = false;
		this->currentMoney += money;
		room->startRent(renter, rentStartDate, rentDays);
		result = logTransaction(money, renter, room);
		return result;
	}
	catch(...)
	{
		return false;
	}
}

string RentController::getDealsHistory()
{
	try
	{
		return getLinesFromFile(this->transactionLogsFilename);
	}
	catch(...)
	{
		return "";
	}
	return "";
}

string RentController::getLastDealHistory()
{
	try
	{
		return getLinesFromFile(this->lastLogFilename);
	}
	catch(...)
	{
		return "";
	}
	return "";
}

string RentController::getLinesFromFile(string filename)
{
	ifstream outputFileStream(filename);
	string finalString = "";

	for(string line; getline(outputFileStream, line); )
	{
		finalString += line + "\n";
	}
	if(finalString == "")
	{
		finalString = "��� �����.";
	}
	return finalString;
}

bool RentController::logTransaction(double money, Renter* renter, Room* room)
{
	try
	{
		time_t currentTime = time(nullptr);
		tm currentDate = *localtime(&currentTime);
		ostringstream stringStream;
		stringStream << put_time(&currentDate, "%d.%m.%Y");
		string currentDateString = stringStream.str();

		string moneyStr = to_string(money);

		ofstream outputFileStream(this->transactionLogsFilename, ios::app);
		outputFileStream << "���������: " + currentDateString << "\n";
		outputFileStream << "���������: \n" + renter->getTextCoreData() << "\n";
		outputFileStream << "��������: \n" + room->getTextCoreData() << "\n";
		outputFileStream << "���� ������ ������: " + ConvertUtills::dateToString(&room->rentStartDate) << "\n";
		outputFileStream << "���� ����� ������: " + ConvertUtills::dateToString(&room->rentEndDate) << "\n";
		outputFileStream << "����� ���� ������: " + to_string(room->rentedDays) << "\n";
		outputFileStream << "� ������: " + moneyStr.substr(0, moneyStr.find_first_of('.') + 3) + "P." << "\n";
		outputFileStream << "-------------------------------------------------------------------------------------\n";
		outputFileStream.close();

		ofstream outputFileStreamLast(this->lastLogFilename);
		outputFileStreamLast << "���������: " + currentDateString << "\n";
		outputFileStreamLast << "���������: \n" + renter->getTextCoreData() << "\n";
		outputFileStreamLast << "��������: \n" + room->getTextCoreData() << "\n";
		outputFileStreamLast << "���� ������ ������: " + ConvertUtills::dateToString(&room->rentStartDate) << "\n";
		outputFileStreamLast << "���� ����� ������: " + ConvertUtills::dateToString(&room->rentEndDate) << "\n";
		outputFileStreamLast << "����� ���� ������: " + to_string(room->rentedDays) << "\n";
		outputFileStreamLast << "� ������: " + moneyStr.substr(0, moneyStr.find_first_of('.') + 2) + "P." << "\n";
		outputFileStreamLast << "-------------------------------------------------------------------------------------\n";
		outputFileStreamLast.close();

		return true;
	}
	catch(...)
	{
		return false;
	}
}

double RentController::getMoney()
{

	return this->currentMoney;
}
