#include "Renter.h"
#include <iostream>
using namespace std;

void Renter::serealize(ostream& ofStream)
{
	int firstNameSize = (this->firstName.size());
	int lastNameSize = (this->lastName.size());
	int fathSize = (this->fath.size());

	ofStream.write((char*) (&this->id), sizeof(int));

	ofStream.write((char*)(&firstNameSize), sizeof(int));
	ofStream.write(this->firstName.c_str(), firstNameSize);

	ofStream.write((char*)(&lastNameSize), sizeof(int));
	ofStream.write(this->lastName.c_str(), lastNameSize);

	ofStream.write((char*)(&fathSize), sizeof(int));
	ofStream.write(this->fath.c_str(), fathSize);
}

Renter* Renter::deserealize(istream& inStream)
{

	int firstNameSize;
	int lastNameSize;
	int fathSize;

	char* buf;

	inStream.read((char*) (&this->id), sizeof(int));

	inStream.read((char*)(&firstNameSize), sizeof(int));
	buf = new char [firstNameSize];
	inStream.read(buf, firstNameSize);
	this->firstName = "";
	this->firstName.append(buf, firstNameSize);

	inStream.read((char*)(&lastNameSize), sizeof(int));
	buf = new char [lastNameSize];
	inStream.read(buf, lastNameSize);
	this->lastName = "";
	this->lastName.append(buf, lastNameSize);

	inStream.read((char*)(&fathSize), sizeof(int));
	buf = new char [fathSize];
	inStream.read(buf, fathSize);
	this->fath = "";
	this->fath.append(buf, fathSize);
	return this;
}

string Renter::getTextCoreData()
{
	string resString = "\t���: " + this->firstName + "\n\t��������: " + this->lastName + "\n\t�������: " + this->fath;
	return resString;
}
