#include "ConvertUtills.h"
#include <regex>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <sstream>

string ConvertUtills::textBoxStringToString(String^ stringToConvert)
{
	try
	{
		char chars [50] = { 0 };
		if(stringToConvert->Length < sizeof(chars))
		{
			sprintf_s(chars, "%s", stringToConvert);
		}
		else
		{
			throw runtime_error("buffer overflow!, buffer size:" + to_string(sizeof(chars)) + "string to convert length:" + to_string(stringToConvert->Length));
		}
		string stlString(chars);
		return stlString;
	}
	catch(...)
	{
		return "";
	}
}

String^ ConvertUtills::stringToTextBoxString(string str)
{
	return gcnew String(str.c_str());
}

String^ ConvertUtills::stringToTextBoxString(string* str)
{
	return gcnew String(str->c_str());
}

String^ ConvertUtills::dateToTextBoxString(tm* date)
{
	return gcnew String(dateToString(date).c_str());
}

tm* ConvertUtills::textBoxStringToDate(String^ stringToConvert)
{
	string date = textBoxStringToString(stringToConvert);

	return stringToDate(date);
}

tm* ConvertUtills::stringToDate(string stringToConvert)
{
	time_t timeNow = time(0);
	tm* convertedDate = new tm();
	localtime_s(convertedDate, &timeNow);
	int day = 0;
	int month = 0;
	int year = 0;

	sscanf_s(stringToConvert.c_str(), "%d.%d.%d", &day, &month, &year);

	convertedDate->tm_mday = day;
	convertedDate->tm_mon = month - 1;
	convertedDate->tm_year = year - 1900;

	return convertedDate;
}

string ConvertUtills::dateToString(tm* date)
{
	try
	{
		ostringstream stringStream;
		stringStream << put_time(date, "%d.%m.%Y");
		string currentDateString = stringStream.str();
		return currentDateString;
	}
	catch(...)
	{
		return "00.00.0000";
	}
}
