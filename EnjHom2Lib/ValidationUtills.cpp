#include "ValidationUtills.h"
#include <regex>

bool ValidationUtills::isNumber(string str)
{
	regex numRegex("\\d+");
	return regex_match(str, numRegex);
}

bool ValidationUtills::isPositiveNumber(string str)
{
	try
	{
		if(str.size() > 0 && str.find_first_not_of("0123456789") == std::string::npos && stoi(str) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch(...)
	{
		return false;
	}
}



bool ValidationUtills::notEmptyString(string str)
{
	return str.length() > 0;
}

bool ValidationUtills::dateIsInCorrectRange(tm* date)
{
	try
	{
		time_t timeNow = time(0);
		time_t dateTime = mktime(date);

		double timeDiff = difftime(dateTime, timeNow);
		int daysDiff = ceil(timeDiff / 86400);

		tm currentDate;

		localtime_s(&currentDate, &timeNow);
		return daysDiff >= 0
			&& date->tm_mday > 0 && date->tm_mday < 32
			&& date->tm_mon >= 0 && date->tm_mon < 12
			&& date->tm_year >= currentDate.tm_year && date->tm_year < currentDate.tm_year + 3;
	}
	catch(...)
	{
		return false;
	}
}

bool ValidationUtills::dateFormatIsCorrect(string str)
{
	regex firstDateRegex("\\d{2}.\\d{2}.\\d{4}");
	if(regex_match(str, firstDateRegex))
	{
		return true;
	}
	return false;
}
