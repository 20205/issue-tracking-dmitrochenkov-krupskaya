#include "Room.h"

void Room::startRent(Renter* renter, tm* rentStartDate, int rentedDays)
{
	this->isRented = true;
	this->rentedDays = rentedDays;
	this->renter = renter;

	time_t timeNow = time(0);
	localtime_s(&this->rentStartDate, &timeNow);
	localtime_s(&this->rentEndDate, &timeNow);
	this->rentEndDate.tm_mday += rentedDays;
	timeNow = mktime(&this->rentEndDate);
	localtime_s(&this->rentEndDate, &timeNow);
}

void Room::endRent()
{
	this->isRented = false;
	this->rentedDays = -1;
	this->renter = NULL;

	int id;
	bool wifi = false;
	int roomsCount = 0;
	int badsCount = 0;
	bool parking = false;
	string address = "";
	int houseNumber = -1;
	int moneyPerMonth = -1;
	Renter* renter = NULL;
	int square = -1;
	bool isRented = false;
	tm rentStartDate = { 0 };
	tm rentEndDate = { 0 };
	int rentedDays = -1;

}

void Room::serealize(ostream& ofStream)
{
	tm tmBuffer;

	ofStream.write((char*) (&this->id), sizeof(int));
	ofStream.write((char*) (&this->wifi), sizeof(bool));
	ofStream.write((char*) (&this->badsCount), sizeof(int));
	ofStream.write((char*) (&this->parking), sizeof(bool));
	ofStream.write((char*) (&this->roomsCount), sizeof(int));

	int addressSize = (this->address.size());
	ofStream.write((char*) (&addressSize), sizeof(int));
	ofStream.write(this->address.c_str(), addressSize);

	ofStream.write((char*) (&this->houseNumber), sizeof(int));
	ofStream.write((char*) (&this->moneyPerMonth), sizeof(int));
	ofStream.write((char*) (&this->square), sizeof(int));
	ofStream.write((char*) (&this->rentedDays), sizeof(int));

	ofStream.write((char*) (&this->isRented), sizeof(bool));

	ofStream.write((char*) &this->rentStartDate, sizeof(tm));
	ofStream.write((char*) &this->rentEndDate, sizeof(tm));

	if(this->renter == NULL)
	{
		Renter* renter = new Renter("", "", "");
		this->renter = renter;
	}
	this->renter->serealize(ofStream);
}

Room* Room::deserealize(istream& inStream)
{
	Renter* renter = new Renter("", "", "");
	tm tmBuffer;

	int addressSize;

	inStream.read((char*) (&this->id), sizeof(int));
	inStream.read((char*) (&this->wifi), sizeof(bool));
	inStream.read((char*) (&this->badsCount), sizeof(int));
	inStream.read((char*) (&this->parking), sizeof(bool));
	inStream.read((char*) (&this->roomsCount), sizeof(int));

	inStream.read((char*) (&addressSize), sizeof(int));
	this->address.resize(addressSize);
	inStream.read(&this->address [0], addressSize);

	inStream.read((char*) (&this->houseNumber), sizeof(int));
	inStream.read((char*) (&this->moneyPerMonth), sizeof(int));
	inStream.read((char*) (&this->square), sizeof(int));
	inStream.read((char*) (&this->rentedDays), sizeof(int));

	inStream.read((char*) (&this->isRented), sizeof(bool));

	inStream.read((char*) &tmBuffer, sizeof(tm));
	this->rentStartDate = tmBuffer;
	inStream.read((char*) &tmBuffer, sizeof(tm));
	this->rentEndDate = tmBuffer;

	this->renter = renter->deserealize(inStream);
	return this;
}

string Room::getTextCoreData()
{
	string resString = "\t�����: " + this->address +
		"\n\t����� ����: " + to_string(this->houseNumber) +
		"\n\t�������: " + to_string(this->square) +
		"\n\t������:" + to_string(this->roomsCount) +
		"\n\t��������:" + to_string(this->badsCount) +
		"\n\twifi:" + (this->wifi ? "��" : "���") +
		"\n\t��������:" + (this->parking ? "��" : "���") +
		"\n\t����� �� �����:" + to_string(this->moneyPerMonth);
	return resString;
}
