#pragma once
#include <string>
#include <istream>
using namespace std;

class Renter
{

public:

	int id;
	string firstName = "";
	string lastName = "";
	string fath = "";


	Renter(string firstName, string lastName, string fath)
	{
		this->id = rand() % 10000;
		this->firstName = firstName;
		this->lastName = lastName;
		this->fath = fath;
	}

	/**
	* Save this Renter to file
	*
	* @param ofStream File stream to save this Renter
	**/
	void serealize(ostream& ofStream);

	/**
	* Load next Renter from file
	*
	* @param ofStream File stream to load next Renter
	*
	* @return Renter instance - Loaded Renter
	**/
	Renter* deserealize(istream& ofStream);

	/**
	* Present core Renter data as text
	*
	* @return core Renter data as text
	**/
	string getTextCoreData();

private:

	Renter() {}
};

